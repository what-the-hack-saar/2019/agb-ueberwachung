### Backend
benoetigt `python3`

```
pip install requirements.txt
```

```python main.py```
um das GUI zu starten

### Frontend

```
npm install create-elm-app -g
```

```
cd elm/
./build.sh
```
um das Frontend zu erstellen und in den www ordner fuer die Python-Anwendung zu kopieren.

