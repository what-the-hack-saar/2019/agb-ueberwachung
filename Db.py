import sqlite3


conn = sqlite3.connect('AGBTracker.db')


def execSql(sql, values):
  c = conn.cursor()
  result = c.execute(sql, values)
  conn.commit()
  return result
  
  
def createTablesIfNotExists():
  execSql('''create table if not exists agb (url text primary key);''',())
  execSql('''
CREATE TABLE if not exists document(
  id      INTEGER PRIMARY KEY,
  url     TEXT NOT NULL, 
  date    TEXT, 
  content TEXT,
  FOREIGN KEY(url) REFERENCES agb(url) ON DELETE CASCADE
);
''', ())

def insertAGB(url):
  execSql('''INSERT INTO agb VALUES(?)''', (url,))
 
def deleteAGB(url):
  execSql('''DELETE
FROM
 agb
WHERE
 url=?;''', (url,))
  
def insertDocument(url, doc):
  execSql('''INSERT INTO document VALUES(NULL, ?,?,?)''', (url,doc["date"],doc["content"]))
  
  
def listAGB():
  return execSql('''
SELECT
 url
FROM
 agb;
    ''',()).fetchall()
      
def getDocument(id):
  return execSql('''
SELECT
 id,
 date,
 content
FROM
 document
WHERE
 id = ?''', (id,)).fetchone()
 
def listDocuments(url):
  return execSql('''
SELECT
 id,
 date,
 content
FROM
 document
WHERE
 url = ? 
GROUP BY content
ORDER BY date
''', (url,)).fetchall()
 
def deleteDocument(id):
  return execSql('''
DELETE
FROM
 document
WHERE
 id = ?''', (id,))