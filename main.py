import eel
import requests
import Download
from datetime import datetime
import Db
    
def fetchDocument(id):
    (id, date, content) = Db.getDocument(id)
    return {
    "id": id,
    "date": date,
    "content": content
    }
    
def fetchDocuments(url):
  return [{
    "id": id,
    "date": date,
    "content": content} for (id,date,content) in Db.listDocuments(url)]
    
def fetchAGB(url):
  docs = fetchDocuments(url)
  return {
    "url": url,
    "last_checked":  docs[-1]["date"] if len(docs) > 0 else "noch nie!",
    "changed": docs[-1]["content"] != docs[-2]["content"] if len(docs)> 1 else False
    }
    
def fetchAGBs():
  agbs = []
  for (url,) in Db.listAGB():
    agbs += [fetchAGB(url)]
  return agbs
    
@eel.expose
def downloadAGB_py(url):
  print ("downloadAGB called with " + url)
  response = requests.get(url)
  response.encoding = "utf-8" 
  downloaded_text = Download.cleanMe (response.text)
  
  new_document = { 
    "date": str(datetime.now())
    , "content": downloaded_text }
  
  Db.insertDocument(url, new_document)
  agb = fetchAGB(url)
  
  print ("passing " + str(agb) + " to JS")
  eel.downloadComplete_js(agb)
    
@eel.expose
def saveNewAGB_py(url):
  print ("saveNewAGB_py called with " + url)
  Db.insertAGB(url)
  eel.saveComplete_js()

@eel.expose
def deleteAGB_py(url):
  print ("deleteAGB_py called with "+url)
  Db.deleteAGB(url)
  eel.deleteComplete_js()

@eel.expose
def changeRoute_py(route):
  print ("changeRoute_py called with "+ str(route))
  if route["page"] == "MainPage":
    agbs = fetchAGBs()
    eel.changeRoute_js({"page": "MainPage"
      , "data": {"agbs": agbs}
      })
  elif route["page"] == "AGBPage":
    eel.changeRoute_js({"page": "AGBPage", "data": {
      "agb": fetchAGB(route["url"]),
      "documents": fetchDocuments(route["url"])
      }})
  elif route["page"] == "DiffPage":
    eel.changeRoute_js({"page": "DiffPage", "data": {
      "agb": fetchAGB(route["url"]),
      "document": fetchDocument(route["id"]),
      "documentPrev": fetchDocument(route["idprev"])
      }})
  
Db.createTablesIfNotExists()
print (Db.listAGB())
  
eel.init('www')
eel.start('index.html')
