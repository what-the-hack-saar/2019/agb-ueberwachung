import './main.css';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';


class Differ extends HTMLElement {
  constructor() {
    const self = super();

    self._doc1 = null;
    self._doc2 = null;

    return self;
  }

  connectedCallback() {
    console.log("differ connected");
  }

  set doc1(doc1) {
    this._doc1 = doc1;
    this.render();
  }

  set doc2(doc2) {
    this._doc2 = doc2;
    this.render();
  }

  render () {
    if (this._doc1 === null || this._doc2 === null) {
      this.innerHTML = "Loading...";
    } else {
      this.innerHTML = htmldiff(this._doc1, this._doc2);
    }
  }
}

window.customElements.define('e-differ', Differ);

/*
var eel = {
  expose: function () {}
  , changeRoute_py: function (route) {
    switch(route.page) {
      case "MainPage":
        changeRoute_js({
          "page": "MainPage"
          , "data": {
            "agbs": [{
              "url": "https://www.otto.de/shoppages/service/agb"
              , "last_checked": "2019-01-12"
              , "changed": false
            }]
          }});
        break;
      case "AGBPage":
        changeRoute_js({
          "page": "AGBPage"
          , "data": {
            "agb": {
              "url": "https://www.wegweag.de/shoppages/service/agb"
              , "last_checked": "2019-01-12"
              , "changed": false
            }
            , "documents": [{"id": 1, "date": "today", "content": "AGB sind doof!"}
            ,  {"id": 2, "date": "later", "content": "AGB sind toll!"}]
          }});
        break;
      case "DiffPage":
          changeRoute_js({
            "page": "DiffPage"
            , "data": {
              "agb": {
                "url": "https://www.wegweag.de/shoppages/service/agb"
                , "last_checked": "2019-01-12"
                , "changed": false
              }
              , "document1": {"id": 1, "date": "today", "content": "AGB sind doof!"}
              , "document2": {"id": 2, "date": "later", "content": "AGB sind toll!"}
            }});
          break;
      default:
        changeRoute_js({
        "page": "MainPagwegwe"
        , "data": {
          "agbs": [{
            "url": "https://www.otto.de/shoppages/service/agb"
            , "last_checked": "2019-01-12"
            , "changed": false
          }]
        }});
      break;
    }
  }
  , downloadAGB_py: function () {
    downloadComplete_js({
      url: url
      , "documents": [{
        "id": 1,
        "date": "2019-01-11 9:31"
        , "content": "AGB sind doof."
      }, {
        "id": 2,
        "date": "2019-01-13 9:33"
        , "content": "AGB sind super."
      }]
      , "last_checked": "2019-01-13 12:33"
      , "changed": false}
    );
  }
  , saveNewAGB_py: function () {}
};

*/
var app = Elm.Main.init({
  node: document.getElementById('root')
});

app.ports.saveNewAGB.subscribe(function(url) {
  console.log(["saveNewAGB", url]);
  eel.saveNewAGB_py(url);
});

app.ports.deleteAGB.subscribe(function(url) {
  console.log(["deleteAGB", url]);
  eel.deleteAGB_py(url);
});

app.ports.downloadAGB.subscribe(function(url) {
  console.log(["downloadAGB", url]);
  eel.downloadAGB_py(url);
});

app.ports.route.subscribe(function (route) {
  console.log(["route", route]);
  eel.changeRoute_py(route)
});

window.saveComplete_js = function saveComplete_js() {
  console.log(["saveComplete_js"]);
  eel.changeRoute_py({"page": "MainPage"});
}

window.deleteComplete_js = function deleteComplete_js() {
  console.log(["deleteComplete_js"]);
  eel.changeRoute_py({"page": "MainPage"});
}

window.downloadComplete_js = function downloadComplete_js(agb) {
  console.log(["agb", agb]);
  app.ports.downloadComplete.send(agb);
}


window.changeRoute_js = function changeRoute_js(model) {
  console.log(["changeRoute_js", model]);
  app.ports.changeRoute.send(model);
}

eel.expose(window.saveComplete_js, "saveComplete_js");
eel.expose(window.deleteComplete_js, "deleteComplete_js");
eel.expose(window.downloadComplete_js, "downloadComplete_js");
eel.expose(window.changeRoute_js, "changeRoute_js");


eel.changeRoute_py({page: "MainPage"});

registerServiceWorker();
