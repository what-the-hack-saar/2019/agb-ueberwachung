port module Main exposing (..)

import Browser
import Html
import Html.Attributes as Attributes
import Html.Events as Events
import Json.Encode as Encode
import Json.Decode as Decode
import Maybe as Maybe


---- PORTS ----


port saveNewAGB : Encode.Value -> Cmd msg


port deleteAGB : Encode.Value -> Cmd msg


port downloadAGB : Encode.Value -> Cmd msg


port downloadComplete : (Encode.Value -> msg) -> Sub msg


port route : Encode.Value -> Cmd msg


port changeRoute : (Encode.Value -> msg) -> Sub msg



--port loadAGB : (Encode.Value -> msg) -> Sub msg


decodeDocument =
    Decode.map3 Document
        (Decode.field "id" Decode.int)
        (Decode.field "date" Decode.string)
        (Decode.field "content" Decode.string)


decodeAGB =
    Decode.map3 AGB
        (Decode.field "url" Decode.string)
        (Decode.field "last_checked" Decode.string)
        (Decode.field "changed" Decode.bool)


decodeAGBState =
    Decode.map2 AGBState
        decodeAGB
        (Decode.succeed False)


decodePage : Decode.Decoder Model
decodePage =
    Decode.field "page" Decode.string
        |> Decode.andThen
            (\page ->
                case page of
                    "MainPage" ->
                        Decode.map MainPage
                            (Decode.field "data"
                                (Decode.field "agbs" (Decode.list decodeAGBState))
                            )

                    "AGBPage" ->
                        Decode.map2 AGBPage
                            (Decode.field "data" (Decode.field "agb" decodeAGB))
                            (Decode.field "data"
                                (Decode.field "documents" (Decode.list decodeDocument))
                            )

                    "DiffPage" ->
                        Decode.map3 DiffPage
                            (Decode.field "data" (Decode.field "agb" decodeAGB))
                            (Decode.field "data"
                                (Decode.field "documentPrev" decodeDocument)
                            )
                            (Decode.field "data"
                                (Decode.field "document" decodeDocument)
                            )

                    _ ->
                        Decode.succeed (ErrorPage "TODO:implement")
            )


mkRouteMsg v =
    case Decode.decodeValue decodePage v of
        Ok page ->
            ChangeRoute page

        Err s ->
            ChangeRoute (ErrorPage (Decode.errorToString s))


mkDownloadCompleteMsg : Encode.Value -> Msg
mkDownloadCompleteMsg v =
    case Decode.decodeValue decodeAGB v of
        Ok agb ->
            MainMsg (DownloadComplete (Just agb))

        Err _ ->
            MainMsg (DownloadComplete Nothing)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ downloadComplete
            mkDownloadCompleteMsg
        , changeRoute
            mkRouteMsg
        ]



---- MODEL ----


type alias Document =
    { id : Int
    , date : String
    , content : String
    }


type alias AGB =
    { url : String
    , last_checked_ : String
    , changed_ : Bool
    }


type alias AGBState =
    { agb : AGB
    , downloading : Bool
    }


type ChangeRoute
    = RouteToMainPage
    | RouteToAGBPage String
    | RouteToDiffPage String Int Int


type Model
    = MainPage (List AGBState)
    | NewAGBPage String
    | AGBPage AGB (List Document)
    | DiffPage AGB Document Document
    | ErrorPage String


init : ( Model, Cmd Msg )
init =
    ( MainPage []
    , Cmd.none
    )



---- UPDATE ----


type MainPageMsg
    = SelectAGB Int
    | DownloadAGB String
    | DownloadComplete (Maybe AGB)
    | DeleteAGB String
    | NewAGB


type NewAGBPageMsg
    = NewAGBUrlChange String
    | CloseNewAGB
    | SaveNewAGB


type Msg
    = NoOp
    | ChangeRoute Model
    | MainMsg MainPageMsg
    | NewAGBMsg NewAGBPageMsg
    | CloseAGB
    | SelectDocument Int Int
    | CloseDocument


getIth i xs =
    List.head (List.drop i xs)


setDownloading url to s =
    { s
        | downloading =
            if s.agb.url == url then
                to
            else
                s.downloading
    }


completeDownload agb s =
    { s
        | downloading =
            if s.agb.url == agb.url then
                False
            else
                s.downloading
        , agb =
            if s.agb.url == agb.url then
                agb
            else
                s.agb
    }


routeTo r =
    let
        v =
            case r of
                RouteToMainPage ->
                    Encode.object
                        [ ( "page", Encode.string "MainPage" )
                        ]

                RouteToAGBPage url ->
                    Encode.object
                        [ ( "page", Encode.string "AGBPage" )
                        , ( "url", Encode.string url )
                        ]

                RouteToDiffPage url idprev id ->
                    Encode.object
                        [ ( "page", Encode.string "DiffPage" )
                        , ( "url", Encode.string url )
                        , ( "id", Encode.int id )
                        , ( "idprev", Encode.int idprev )
                        ]
    in
        route v


updateMainPage msg model =
    case model of
        MainPage agbs ->
            case msg of
                SelectAGB i ->
                    case getIth i agbs of
                        Nothing ->
                            ( model, Cmd.none )

                        Just s ->
                            ( model, routeTo (RouteToAGBPage s.agb.url) )

                DownloadAGB url ->
                    let
                        newagbs =
                            List.map
                                (setDownloading url True)
                                agbs
                    in
                        ( MainPage newagbs, downloadAGB (Encode.string url) )

                DownloadComplete maybeAgb ->
                    case maybeAgb of
                        Nothing ->
                            ( model, Cmd.none )

                        Just agb ->
                            let
                                newagbs =
                                    List.map
                                        (completeDownload agb)
                                        agbs
                            in
                                ( MainPage newagbs, Cmd.none )

                DeleteAGB url ->
                    ( model, deleteAGB (Encode.string url) )

                NewAGB ->
                    ( NewAGBPage "", Cmd.none )

        _ ->
            ( model, Cmd.none )


updateNewAGBPage msg model =
    case msg of
        NewAGBUrlChange url ->
            ( NewAGBPage url, Cmd.none )

        CloseNewAGB ->
            ( model, routeTo RouteToMainPage )

        SaveNewAGB ->
            let
                cmd =
                    case model of
                        NewAGBPage url ->
                            case url of
                                "" ->
                                    Cmd.none

                                _ ->
                                    saveNewAGB (Encode.string url)

                        _ ->
                            routeTo RouteToMainPage
            in
                ( model, cmd )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ChangeRoute m ->
            ( m, Cmd.none )

        MainMsg mainPageMsg ->
            case model of
                MainPage agbs ->
                    updateMainPage mainPageMsg model

                _ ->
                    ( model, Cmd.none )

        NewAGBMsg newAGBPageMsg ->
            case model of
                NewAGBPage _ ->
                    updateNewAGBPage newAGBPageMsg model

                _ ->
                    ( model, Cmd.none )

        CloseAGB ->
            ( model, routeTo RouteToMainPage )

        SelectDocument idprev id ->
            case model of
                AGBPage agb documents ->
                    ( model, routeTo (RouteToDiffPage agb.url idprev id) )

                _ ->
                    ( model, routeTo RouteToMainPage )

        CloseDocument ->
            case model of
                DiffPage agb _ _ ->
                    ( model, routeTo (RouteToAGBPage agb.url) )

                _ ->
                    ( model, routeTo RouteToMainPage )



---- VIEW ----


zip : List a -> List b -> List ( a, b )
zip a b =
    case a of
        [] ->
            []

        x :: xs ->
            case b of
                [] ->
                    []

                y :: ys ->
                    ( x, y ) :: (zip xs ys)


firstDate document =
    document.date


navButton msg text =
    Html.a [ Attributes.class "btn", Events.onClick msg ] [ Html.text text ]


backButton msg =
    navButton msg "Zurück"


actionButton msg text =
    Html.button [ Attributes.class "btn-primary", Events.onClick msg ] [ Html.text text ]


saveButton msg =
    actionButton msg "Speichern"


viewAGBli : Int -> AGBState -> Html.Html Msg
viewAGBli index s =
    Html.li []
        [ navButton (MainMsg (SelectAGB index)) s.agb.url
        , Html.span []
            [ Html.strong [] [ Html.text " zuletzt geprüft: " ]
            , Html.text s.agb.last_checked_
            , Html.text
                (if s.agb.changed_ then
                    " (geändert)"
                 else
                    ""
                )
            ]
        , Html.br [] []
        , if s.downloading then
            Html.text " Downloading... "
          else
            actionButton (MainMsg (DownloadAGB s.agb.url)) "Herunterladen"
        , actionButton (MainMsg (DeleteAGB s.agb.url)) "Löschen"
        ]


viewDocumentli : ( Maybe Document, Document ) -> Html.Html Msg
viewDocumentli ( maybeDocumentPrev, document ) =
    case maybeDocumentPrev of
        Nothing ->
            Html.li [] [ navButton (SelectDocument document.id document.id) (firstDate document) ]

        Just documentPrev ->
            Html.li [] [ navButton (SelectDocument documentPrev.id document.id) (firstDate document) ]


view : Model -> Html.Html Msg
view model =
    case model of
        MainPage agbs ->
            Html.div []
                [ Html.h1 [] [ Html.text "AGB Überwacher" ]
                , Html.p [] [ Html.text "Hier kannst du die AGBs von verschiedenen Webseiten auf Veränderungen überwachen." ]
                , Html.ul [] (List.indexedMap viewAGBli agbs)
                , navButton (MainMsg NewAGB) "Neue Überwachung"
                ]

        NewAGBPage url ->
            Html.div []
                [ Html.h1 [] [ Html.text "Welche URL soll überwacht werden?" ]
                , Html.form []
                    [ Html.input [ Attributes.value url, Events.onInput (NewAGBMsg << NewAGBUrlChange) ] []
                    ]
                , backButton (NewAGBMsg CloseNewAGB)
                , saveButton (NewAGBMsg SaveNewAGB)
                ]

        AGBPage agb documents ->
            let
                zipped_documents =
                    zip ([ Nothing ] ++ (List.map Just documents)) documents
            in
                Html.div []
                    [ Html.h1 [] [ Html.text agb.url ]
                    , Html.ul [] (List.map viewDocumentli zipped_documents)
                    , backButton CloseAGB
                    ]

        DiffPage agb documentPrev document ->
            Html.div []
                [ Html.h1 []
                    [ Html.text ("Version vom " ++ firstDate document)
                    ]
                , backButton CloseDocument
                , Html.br [] []
                , Html.node "e-differ"
                    [ Attributes.property "doc1" (Encode.string documentPrev.content)
                    , Attributes.property "doc2" (Encode.string document.content)
                    ]
                    []
                , Html.br [] []
                , backButton CloseDocument
                ]

        ErrorPage t ->
            Html.div [] [ Html.p [] [ Html.text t ] ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }
